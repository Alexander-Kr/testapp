import React from 'react';
import AppContent from './src';

import { View, Text } from 'react-native';

class App extends React.Component {
  render() {
    return (
      <AppContent />
    );
  }
}

export default App;