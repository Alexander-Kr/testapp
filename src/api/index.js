import {url, key, count} from './conf';


export const fetchPhotos = async () => {
    return await (await (fetch(`${url}photos/random?count=${count}&client_id=${key}`)
            .then(response => response.json())
            .catch(err => {
                console.log('Error: ', err);
            })
    ));
};