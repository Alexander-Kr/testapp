export const getPhotoById = async (state, id) => state.reducers.photos.find(photo => photo.id === id);
