import {
    FETCH_PHOTOS_START,
    FETCH_PHOTOS_SUCCESS,
    FETCH_PHOTOS_FAILURE,
    FETCH_MORE_PHOTOS_START,
    FETCH_MORE_PHOTOS_SUCCESS,
    FETCH_MORE_PHOTOS_FAILURE,
    LOAD_PHOTO_BY_ID_START,
    LOAD_PHOTO_BY_ID_SUCCESS,
    LOAD_PHOTO_BY_ID_FAILURE
} from '../actionTypes';

import {
    fetchPhotos as fetchPhotosApi
} from '../api';

import {getPhotoById} from '../selectors';

export const fetchPhotos = () => async dispatch => {
    dispatch({type: FETCH_PHOTOS_START});
    try {
        const photos = await fetchPhotosApi();
        dispatch({
            type: FETCH_PHOTOS_SUCCESS,
            payload: photos
        });
    } catch (err) {
        dispatch({
            type: FETCH_PHOTOS_FAILURE,
            payload: err,
            error: true,
        });
    }
};

export const loadPhotoById = id => async (dispatch, getState) => {
    dispatch({type: LOAD_PHOTO_BY_ID_START});
    try {
        const photo = await getPhotoById(getState(), id);
        dispatch({
            type: LOAD_PHOTO_BY_ID_SUCCESS,
            payload: photo
        });
    } catch (err) {
        dispatch({
            type: LOAD_PHOTO_BY_ID_FAILURE,
            payload: err,
            error: true,
        });
    }
};

export const fetchMorePhotos = () => async dispatch => {
    dispatch({type: FETCH_MORE_PHOTOS_START});
    try {
        const photos = await fetchPhotosApi();
        dispatch({
            type: FETCH_MORE_PHOTOS_SUCCESS,
            payload: photos
        });
    } catch (err) {
        dispatch({
            type: FETCH_MORE_PHOTOS_FAILURE,
            payload: err,
            error: true,
        });
    }
};
