import React from 'react';

import {View, Text, Button} from 'react-native';

export default class InfoAboutApp extends React.Component {
    render() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontSize: 30, textAlign: 'center', marginBottom: 10}}>Приложение создано для тестового
                    задания Devsteam.mobi</Text>
                <Button
                    onPress={() => this.props.navigation.goBack()}
                    title="Назад!"
                />
            </View>
        );
    }
}