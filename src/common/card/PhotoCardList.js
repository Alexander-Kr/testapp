import React from 'react';
import {Dimensions, Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';

class PhotoCardList extends React.Component {
    render() {
        const {user, urls} = this.props.data;

        return (
            <View style={styles.wrapperPhoto}>
                <TouchableOpacity
                    onPress={() => this.props.navigate('AboutPhoto', {id: this.props.data.id})}>
                    <Text>{user.name}</Text>
                    <Image
                        style={styles.photo}
                        source={{uri: urls.regular}}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
    photo: {
        width: DEVICE_WIDTH,
        height: DEVICE_HEIGHT / 2,
        alignSelf: 'stretch'
    },
    wrapperPhoto: {
        marginBottom: 10
    }
});

export default PhotoCardList;