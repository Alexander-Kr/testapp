import React from 'react';
import {Dimensions, View, Image, StyleSheet, TouchableOpacity} from 'react-native';

class PhotoCardCell extends React.Component {
    render() {
        const {urls} = this.props.data;

        return (
            <View>
                <TouchableOpacity
                    onPress={() => this.props.navigate('AboutPhoto', {id: this.props.data.id})}>
                    <Image
                        style={styles.photo}
                        source={{uri: urls.thumb}}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const styles = StyleSheet.create({
    photo: {
        width: DEVICE_WIDTH / 4 - 4,
        height: DEVICE_WIDTH / 4 - 4,
        alignSelf: 'stretch',
        margin: 2,
        minWidth: 80,
        minHeight: 80
    }
});


export default PhotoCardCell;