import React from 'react';
import {connect} from 'react-redux';
import {
    Button,
    View,
    ScrollView,
    StyleSheet,
    Text,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';

import {fetchPhotos, fetchMorePhotos} from '../actions';

import PhotoCardList from '../common/card/PhotoCardList';
import PhotoCardCell from '../common/card/PhotoCardCell';

class GalleryScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    state = {
        mode: 'list'
    };
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Gallery',
            headerRight: (
                <Button
                    onPress={() => navigation.navigate('InfoAboutApp')}
                    title="О приложении"
                />
            ),
        };
    };

    componentDidMount() {
        this.props.fetchPhotos();
    }

    changeModeToList = () => {
        this.setState({mode: 'list'});
    };
    changeModeToCells = () => {
        this.setState({mode: 'cells'});
    };

    render() {
        const photos = this.props.photos;

        return (
            <ScrollView>
                <View style={styles.btnsChangeView}>
                    <View style={styles.btnChangeView}>
                        <Button
                            title="Tab 1"
                            onPress={this.changeModeToList}
                        />
                    </View>
                    <View style={styles.btnChangeView}>
                        <Button
                            title="Tab 2"
                            onPress={this.changeModeToCells}
                        />
                    </View>
                </View>
                {
                    photos.length
                        ?
                        <View style={styles.photosWrapper}>
                            {
                                this.state.mode === 'list' ?
                                    this.props.photos.map(photo => <PhotoCardList key={photo.id} data={photo}
                                                                                  navigate={this.props.navigation.navigate}/>)
                                    :
                                    this.props.photos.map(photo => <PhotoCardCell key={photo.id} data={photo}
                                                                                  navigate={this.props.navigation.navigate}/>)
                            }
                        </View>
                        :
                        <View style={[styles.loader]}>
                            <ActivityIndicator size="large" color="#0000ff"/>
                        </View>
                }
                <View>
                    <Button
                        title='Загрузить ещё...'
                        onPress={this.props.fetchMorePhotos}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    btnsChangeView: {
        flex: 1,
        flexGrow: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    btnChangeView: {
        margin: 10
    },
    photosWrapper: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 20
    },
    loader: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});


const mapStateToProps = state => {
    return {
        photos: state.reducers.photos,
    }
};
const mapDispatchToProps = {
    fetchPhotos,
    fetchMorePhotos
};
export default connect(mapStateToProps, mapDispatchToProps)(GalleryScreen);
