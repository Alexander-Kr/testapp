import React from 'react';
import {connect} from 'react-redux';

import {loadPhotoById} from '../actions';

import {View, ScrollView, ActivityIndicator, Image, StyleSheet, Dimensions, Button} from 'react-native';

class AboutPhotoScreen extends React.Component {
    componentDidMount() {
        this.props.loadPhotoById(this.props.navigation.getParam('id'));
    }

    render() {
        const {urls} = this.props.photo;
        return (
            <ScrollView>
                {
                    Object.entries(this.props.photo).length
                        ?
                        <View>
                            <Image
                                style={styles.photo}
                                source={{uri: urls.regular}}
                            />
                        </View>
                        :
                        <ActivityIndicator size="large" color="#0000ff"/>
                }
            </ScrollView>
        );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
    photo: {
        width: DEVICE_WIDTH - 4,
        height: DEVICE_HEIGHT - 82,
        margin: 2
    }
});

const mapStateToProps = state => {
    console.log('state', state);
    return {
        photo: state.reducers.photo
    }
};

const mapDispatchToProps = {
    loadPhotoById
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutPhotoScreen);