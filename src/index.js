import React from 'react';
import {Provider, connect} from 'react-redux';
import thunk from 'redux-thunk';
import {
    createStore,
    applyMiddleware,
    combineReducers,
} from 'redux';

import {createStackNavigator} from 'react-navigation';
import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
} from 'react-navigation-redux-helpers';


import GalleryScreen from './tabs/GalleryScreen';
import AboutPhotoScreen from './tabs/AboutPhotoScreen';
import InfoAboutApp from './common/modals/InfoAboutApp';
import reducers from './reducers';

const MainStack = createStackNavigator(
    {
        Gallery: {
            screen: GalleryScreen,
        },
        AboutPhoto: {
            screen: AboutPhotoScreen,
        },
    }
);

const RootStack = createStackNavigator(
    {
        Main: {
            screen: MainStack,
        },
        InfoAboutApp: {
            screen: InfoAboutApp,
        },
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
);


const navReducer = createNavigationReducer(RootStack);
const appReducer = combineReducers({
    nav: navReducer,
    reducers
});

const navigationMiddleware = createReactNavigationReduxMiddleware(
    'root',
    (state) => state.nav
);
const middlewares = [navigationMiddleware, thunk];

const App = reduxifyNavigator(RootStack, "root");
const mapStateToProps = (state) => ({
    state: state.nav,
});
const AppWithNavigationState = connect(mapStateToProps)(App);


const store = createStore(
    appReducer,
    applyMiddleware(...middlewares),
);

export default class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AppWithNavigationState/>
            </Provider>
        );
    }
}