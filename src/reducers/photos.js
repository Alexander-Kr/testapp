import {
    FETCH_PHOTOS_SUCCESS,
    FETCH_MORE_PHOTOS_SUCCESS
} from '../actionTypes';

const initialState = [];

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case FETCH_PHOTOS_SUCCESS:
            return payload;
        case FETCH_MORE_PHOTOS_SUCCESS:
            return [...state, ...payload];
        default:
            return state;
    }
}