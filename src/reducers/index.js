import {combineReducers} from 'redux';
import photos from './photos';
import photo from './photo';

export default combineReducers({
    photos,
    photo
});
