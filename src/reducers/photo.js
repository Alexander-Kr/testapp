import {
    LOAD_PHOTO_BY_ID_SUCCESS,
} from '../actionTypes';

const initialState = {};

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case LOAD_PHOTO_BY_ID_SUCCESS:
            return payload;
        default:
            return state;
    }
}
